import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './TopBar.css';

class TopBar extends Component {

	render() {
		return (
			<header className="TopBar">
				<section className="TopBar__Contents">
					<header className="TopBar__Header">
						<Link to="/worlds" className="TopBar__Header__World">
							<h1 className="TopBar__Header__World__Title">
								<span className="fa fa-arrow-left" aria-hidden="true"></span>
								World Name</h1>
						</Link>
						<NavLink
							to="/search"
							className="TopBar__Header__Search TopBar__Navigation__Link"
							activeClassName="TopBar__Navigation__Link--activated">
							<span className="fa fa-search" aria-hidden="true"></span>
						</NavLink>
						<NavLink 
							to="/settings" 
							className="TopBar__Header__Settings TopBar__Navigation__Link" 
							activeClassName="TopBar__Navigation__Link--activated">
							<span className="fa fa-ellipsis-v" aria-hidden="true"></span>
						</NavLink>
					</header>
					<nav className="TopBar__Navigation">
						<menu>
							<li><NavLink
									to="/note/info"
									className="TopBar__Navigation__Link"
									activeClassName="TopBar__Navigation__Link--activated">
									<span className="fa fa-address-card" aria-hidden="true"></span>
							</NavLink></li>
							<li><NavLink
									to="/note/files"
									className="TopBar__Navigation__Link"
									activeClassName="TopBar__Navigation__Link--activated">
									<span className="fa fa-picture-o" aria-hidden="true"></span>
							</NavLink></li>
							<li><NavLink
									to="/note/relationships"
									className="TopBar__Navigation__Link"
									activeClassName="TopBar__Navigation__Link--activated">
									<span className="fa fa-chain" aria-hidden="true"></span>
							</NavLink></li>
						</menu>
					</nav>
					<section className="TopBar__Cancel_Confirm">
						<menu>
							<li>
								<button className="TopBar__Cancel_Confirm__Selection">
									<span className="fa fa-times" aria-hidden="true"></span>
								</button>
							</li>
							<li>
								<button className="TopBar__Cancel_Confirm__Selection">
									<span className="fa fa-check" aria-hidden="true"></span>
								</button>
							</li>
						</menu>
					</section>
				</section>
			</header>
		);
	}
}

export default TopBar;
