import React, { Component } from 'react';
import {
	BrowserRouter as Router,
	Route
} from 'react-router-dom';
import TopBar from '../TopBar';
import Search from '../../views/Search/';
import InfoTab from '../../views/NotePage/InfoTab/';
import FilesTab from '../../views/NotePage/FileTab/';
import RelationshipsTab from '../../views/NotePage/RelationshipsTab/';
import MapPage from '../../views/MapPage/';
import Settings from '../../views/Settings/';
import WorldsPage from '../../views/WorldsPage/';
import Login from '../../views/Login/';

import './Routes.css';

class Routes extends Component {
	render() {
		return (
			<Router>
				<section className="App">
					<section className="App__TopBar">
						<TopBar></TopBar>
					</section>
					<section className="App__Content">
						<Route exact path="/" component={Login}/>
						<Route path="/search" component={Search}/>
						<Route path="/note/info" component={InfoTab}/>
						<Route path="/note/files" component={FilesTab}/>
						<Route path="/note/relationships" component={RelationshipsTab}/>
						<Route path="/map" component={MapPage}/>
						<Route path="/settings" component={Settings}/>
						<Route path="/worlds" component={WorldsPage}/>
					</section>
				</section>
			</Router>
		);
	}
}

export default Routes;
