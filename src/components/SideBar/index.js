import React, { Component } from 'react';
import './SideBar.css';

class SideBar extends Component {
	constructor(props){
		super(props);
		this.state = {
			SideBar__Contents:"SideBar__Contents SideBar__Contents--hidden"
		};

		this.toggleDisplayed = this.toggleDisplayed.bind(this);
	}

	render() {
		return (
			<section className="SideBar">
				<header className="SideBar__Header" onClick={this.toggleDisplayed}>{this.props.header}</header>
				<div className={this.state.SideBar__Contents}>
					{this.props.children}
				</div>
			</section>
		);
	}

	toggleDisplayed () {
		if(this.state.SideBar__Contents === "SideBar__Contents SideBar__Contents--hidden"){
			this.setState({SideBar__Contents:"SideBar__Contents"});
		}else{
			this.setState({SideBar__Contents:"SideBar__Contents SideBar__Contents--hidden"});
		}
	}
}

export default SideBar;
