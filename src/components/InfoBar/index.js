import React, { Component } from 'react';
import './InfoBar.css';

class InfoBar extends Component {
	constructor(props){
		super(props);
		this.state = {
			InfoBar__Contents:"InfoBar__Contents InfoBar__Contents--hidden"
		};

		this.toggleDisplayed = this.toggleDisplayed.bind(this);
	}

	render() {
		return (
			<section className="SideBar">
				<header className="InfoBar__Header" onClick={this.toggleDisplayed}>{this.props.header}</header>
				<div className={this.state.InfoBar__Contents}>
					{this.props.children}
				</div>
			</section>
		);
	}

	toggleDisplayed () {
		if(this.state.InfoBar__Contents === "InfoBar__Contents InfoBar__Contents--hidden"){
			this.setState({InfoBar__Contents:"InfoBar__Contents"});
		}else{
			this.setState({InfoBar__Contents:"InfoBar__Contents InfoBar__Contents--hidden"});
		}
	}
}

export default InfoBar;
