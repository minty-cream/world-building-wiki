import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './NoteLink.css';

class NoteLink extends Component {
	render() {
		return (
			<section className="NoteLink">
				<Link to="/note" className="NoteLink__Contents">
					<div className="NoteLink__Contents__Image">
						<img src={this.props.imgSrc} alt="Display Icon"/>
					</div>
					<div className="NoteLink__Contents__Description">
						{this.props.children}
					</div>
				</Link>
			</section>
		);
	}
}

export default NoteLink;
