import React, { Component } from 'react';
import './Relationships.css';

class Relationships extends Component {
	render() {
		return (
			<section className="Relationships">
				<div className="Relationships__Contents">
					<ul className="Relationships__Contents__Relationships">
						{[...Array(10)].map((x, i) =>
						<li>
							<section className="Relationship__Contents__Relationships__Relationship">
								<h4>Boletaria</h4>
								<img src="http://lorempixel.com/200/200" />
								<p>Relationships</p>
								<ul>
									<li>
										<p>
											Homeland&nbsp;
											<button className="Relationship__Contents__Relationships__Relationship__Delete"><span className="fa fa-trash" aria-hidden="true"></span></button>
										</p>
									</li>
									<li>
										<p>
											Defended&nbsp;
											<button className="Relationship__Contents__Relationships__Relationship__Delete"><span className="fa fa-trash" aria-hidden="true"></span></button>
										</p>
									</li>
								</ul>
								<label htmlFor="add_relationship">Add Relationship</label>
								<input type="text" id="add_relationship" name="add_relationship" placeholder="Please tell me how this card is related now." />
								<button>Add Relationship</button>
								<button className="Relationship__Contents__Relationships__Relationship__Delete">Delete All Relationships</button>
							</section>
						</li>
						)}
					</ul>

				</div>
			</section>
		);
	}
}

export default Relationships;
