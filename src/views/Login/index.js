import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './Login.css';

class Login extends Component {
	render() {
		return (
			<main className="Login">
				<section className="Login__Contents">
					<header><h2>Login or Register</h2></header>
					<p>
						Hi, this is a wiki made to help you build and document worlds. It has a map, a notebook, and a relationship tree.
					</p>
					<p>
						Note: This is a mockup. The links in the top bar will take you places. Nothing actually does anything to any sort of backend.
					</p>
					<p>
						Also note: The login form below me is purely for display purposes only.
					</p>
					<form className="Login__Contents__Form">
						<label htmlFor="email">Email</label>
						<input type="email" name="email" id="password"/>
						<label htmlFor="password">Password</label>
						<input type="password" name="password" id="password"/>
						<button>Login</button>
						<button>Register</button>
					</form>
				</section>
			</main>
		);
	}
}

export default Login;
