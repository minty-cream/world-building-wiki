import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Search.css';

class Search extends Component {
	render() {
		return (
			<main className="Search">
				<aside className="Search__Search">
					<h3>Search</h3>
					<label htmlFor="title">Title</label>
					<input name="title" id="title" type="text"/>
					<label htmlFor="type">Type</label>
					<input name="type" id="type" type="text"/>
					<label htmlFor="keywords">Keywords</label>
					<input name="keywords" id="keywords" type="text"/>
					<label htmlFor="tags">Tags</label>
					<input name="tags" id="tags" type="text"/>
					<button>Search</button>
				</aside>
				<section className="Search__NoteLinkList">
					<ul className="Search__NoteLinkList__List">
						{[...Array(10)].map((x, i) =>
							<li>
								<section className="Search__NoteLinkList__List__Result">
									<Link to="/note">
										<div><img src="http://lorempixel.com/200/200/nature/"/></div>
										<h3>Name Of Note</h3>
										<p>Short Paragraph AAA AAA AA AA AA AA AA AA AA AA AA AA AA</p>
									</Link>
								</section>
							</li>
						)}
					</ul>
				</section>
			</main>
		);
	}
}

export default Search;
