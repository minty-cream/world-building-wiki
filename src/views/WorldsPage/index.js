import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './WorldsPage.css';

class WorldsPage extends Component {
	render() {
		return (
			<main className="WorldsPage">
				<section className="WorldsPage__Contents">
					<ul className="WorldsPage__Contents__List">
						{[...Array(10)].map((x, i) =>
							<li>
								<section className="WorldsPage__Contents__List__World">
									<Link to="/map">
										<div><img src="http://lorempixel.com/200/200/nature/"/></div>
										<h3>Name Of World</h3>
										<p>Short Paragraph AAA AAA AA AA AA AA AA AA AA AA AA AA AA</p>
									</Link>
								</section>
							</li>
						)}
					</ul>
				</section>
			</main>
		);
	}
}

export default WorldsPage;
