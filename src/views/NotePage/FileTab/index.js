import React, { Component } from 'react';

import './FileTab.css';

class FileTab extends Component {
	render() {
		return (
			<main className="FileTab">
				<section className="FileTab__Content__Upload_File">
					<h3>Add File</h3>
					<form>
						<label htmlFor="file">File</label>
						<input type="file" id="file" name="file"/>
						<label htmlFor="description">Description</label>
						<input type="text" id="description" name="description" placeholder="A few simple words to help you remember why you uploaded this image"/>
						<button>Add File</button>
					</form>
				</section>
				<section className="FileTab__Content__File_List">
					<h3>Files</h3>
					<ul>
						<li>
							<img src="http://lorempixel.com/200/200"/>
							<p>Small description about image AAA AAA AAA AAA AAA AAA AAA AAA</p>
							<button className="FileTab__Content__File_List__Select_Cover">Make Cover Image</button>
							<button className="FileTab__Content__File_List__Delete_Image">Delete</button>
						</li>
					</ul>
				</section>
			</main>
		);
	}
}

export default FileTab;
