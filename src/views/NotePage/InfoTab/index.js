import React, { Component } from 'react';

import './InfoTab.css';

class InfoTab extends Component {
	render() {
			return (
				<main className="InfoTab">
					<section className="InfoTab__Content__Name">
						<h3>Name</h3>
						<input type="text" placeholder="Change name..."/>
					</section>
					<section className="InfoTab__Content__Details">
						<h3>Details</h3>
						<textarea placeholder="Edit the description..."></textarea>
					</section>
					<section className="InfoTab__Content__Tags">
						<h3>Tags</h3>
						<input type="text" id="tags" name="tags" placeholder="Add, some, tags" />
					</section>
					<section className="InfoTab__Content__Image">
						<img src="http://lorempixel.com/200/200/"/>
					</section>
				</main>
			);
	}
}

export default InfoTab;
