import React, { Component } from 'react';
import Relationships from '../../../components/Relationships/';

import './RelationshipsTab.css';

class RelationshipsTab extends Component {
	render() {
		return (
			<main className="RelationshipsTab">
				<section className="RelationshipsTab__Add_Relationship">
					<h3>Add Relationship</h3>
					<form>
						<label htmlFor="name">Who is this card related to?</label>
						<input type="text" id="name" name="name" placeholder="Write their name in this box."/>
						<label htmlFor="description">How is this card related?</label>
						<input type="text" id="description" name="description" placeholder="Answer that question with a meaningful description here, please."/>
						<button>Add Relationship</button>
					</form>
				</section>
				<Relationships></Relationships>
			</main>
		);
	}
}

export default RelationshipsTab;
