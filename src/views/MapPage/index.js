import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Draggable from 'react-draggable';
import SideBar from '../../components/SideBar';
import InfoBar from '../../components/InfoBar';

import GrassTile from '../../assets/Tiles/Terrain/Grass/grass_01.png';

import './MapPage.css';

class MapPage extends Component {
	render() {
		return (
			<main className="MapPage">
				<section className="MapPage__Content">
					<Draggable>
						<div className="MapPage__Content__Map">
							<div className="MapPage__Content__Map__Row MapPage__Content__Map__Row--even">
								<Link to="/note/info"><img src={GrassTile}/></Link>
								<Link to="/note/info"><img src={GrassTile}/></Link>
								<Link to="/note/info"><img src={GrassTile}/></Link>
							</div>
							<div className="MapPage__Content__Map__Row MapPage__Content__Map__Row--odd">
								<Link to="/note/info"><img src={GrassTile}/></Link>
								<Link to="/note/info"><img src={GrassTile}/></Link>
								<Link to="/note/info"><img src={GrassTile}/></Link>
							</div>
						</div>
					</Draggable>
				</section>
			</main>
		);
	}
}

export default MapPage;
