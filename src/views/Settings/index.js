import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Settings.css';

class Settings extends Component {
	render() {
		return (
			<main className="Settings">
				<section className="Settings__Contents">
					<ul className="Settings__Contents__Options">
						<li><Link to="/">Log Out</Link></li>
						<li>Change World Cover Image</li>
						<li>Rename World</li>
					</ul>
				</section>
			</main>
		);
	}
}

export default Settings;
